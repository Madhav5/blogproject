from django.shortcuts import render

# Create your views here.
from django.views.generic import *
from .models import *
from .forms import *

class HomeView(TemplateView):
	template_name="home.html"



class ContactView(FormView):
	template_name="contact.html"
	form_class=ContactForm
	success_url="/"


	def form_valid(self,form):
		nam=form.cleaned_data["fullname"]
		eml=form.cleaned_data["email"]
		mbl=form.cleaned_data["mobile"]
		sub=form.cleaned_data['subject']
		message=form.cleaned_data['message']
		Message.objects.create(sender=nam,email=eml,mobile=mbl,subject=sub,text=message)

		return super().form_valid(form)


class AboutView(TemplateView):
	template_name="about.html"


class GalleryView(TemplateView):
	template_name="gallery.html"

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['allphotos'] = Photo.objects.all().order_by('-id')
		
		return context
	



class PhotoDetailView(DetailView):
	template_name="photodetail.html"
	model=Photo
	context_object_name="photoobject"


# class EventView(TemplateView):
# 	template_name="event.html"

class BlogListView(ListView):
	template_name="blog.html"
	queryset=Blog.objects.all().order_by("-id")
	context_object_name="allblogs"



class BlogDetailView(DetailView):
	template_name="blogdetail.html"
	model=Blog
	context_object_name="blogobject"

class CareerView(TemplateView):
	template_name="career.html"


# class SurveyView(FormView):
# 	template_name="survey.html"
# 	form_class=SurveyForm
# 	success_url="/"


# 	def form_valid(self,form):
# 		nam=form.cleaned_data["fullname"]
# 		eml=form.cleaned_data["email"]
# 		mbl=form.cleaned_data["mobile"]
# 		sub=form.cleaned_data['subject']
# 		message=form.cleaned_data['message']
# 		Message.objects.create(sender=nam,email=eml,mobile=mbl,subject=sub,text=message)

# 		return super().form_valid(form)



class ServicesView(TemplateView):
	template_name="services.html"


class ProductsView(TemplateView):
	template_name="products.html"


class BlogCreateView(CreateView):
	template_name="create.html"
	form_class=BlogForm
	success_url="/blog/"


class PostListView(ListView):
	template_name="post.html"
	queryset=Post.objects.all().order_by("-id")
	context_object_name="allposts"



class BlogUpdateView(UpdateView):
	template_name="create.html"
	form_class=BlogForm
	success_url="/blog/"
	model=Blog
	queryset=Blog.objects.all()



class PostDetailView(DetailView):
	template_name="postdetail.html"
	model=Post
	context_object_name="postobject"



class PostUpdateView(UpdateView):
	template_name="postcreate.html"
	form_class=PostForm
	success_url="/post/"
	model=Post
	queryset=Post.objects.all()



class PostCreateView(CreateView):
	template_name="postcreate.html"
	form_class=PostForm
	success_url="/post/"




class PhotoCreateView(CreateView):
	template_name="photocreate.html"
	form_class=PhotoForm
	success_url="/gallery/"


class RegistrationView(FormView):
	template_name="register.html"
	form_class=RegistrationForm
	success_url="/"
