from django.db import models

from django.contrib.auth.models import User



STATUS = (
    (0,"Draft"),
    (1,"Publish")
)

class Post(models.Model):
    title = models.CharField(max_length=200, unique=True)
    author = models.ForeignKey(User, on_delete= models.CASCADE,related_name='blog_posts')
    updated_on = models.DateTimeField(auto_now_add= True)
    content = models.TextField()
    created_on = models.DateTimeField(auto_now_add=True)
    status = models.IntegerField(choices=STATUS, default=0)

    class Meta:
        ordering = ['-created_on']

    def __str__(self):
        return self.title


class Blog(models.Model):
    title=models.CharField(max_length=500)
    image=models.ImageField(upload_to="blogs")
    description=models.TextField()
    author=models.CharField(max_length=50)
    published_on=models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        return self.title


class Photo(models.Model):
    title=models.CharField(max_length=200)
    image=models.ImageField(upload_to="photos")

    def __str__(self):
        return self.title



class Message(models.Model):
    sender=models.CharField(max_length=200)
    email=models.EmailField(null=True,blank=True)
    mobile=models.CharField(max_length=50)
    subject=models.CharField(max_length=200)
    text=models.TextField()
    date=models.DateTimeField(auto_now_add=True)


    def __str__(self):
        return self.sender