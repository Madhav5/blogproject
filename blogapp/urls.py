from django.urls import path
from .views import *


app_name="blogapp"
urlpatterns=[
	path("",HomeView.as_view()),
	path("contact/",ContactView.as_view()),
	path("about/",AboutView.as_view()),
	path("gallery/",GalleryView.as_view()),
	path("services/",ServicesView.as_view()),
	path("register/",RegistrationView.as_view(),name="registration"),
	#path("event/",PostView.as_view()),
	path("blog/",BlogListView.as_view()),
	path("career/",CareerView.as_view()),
	# path("survey/",SurveyView.as_view()),
	path("products/",ProductsView.as_view()),
	path("blog/<int:pk>/detail/",BlogDetailView.as_view()),
	path("create/",BlogCreateView.as_view()),
	path("create/post/",PostCreateView.as_view()),
	path("post/<int:pk>/detail/",PostDetailView.as_view()),
	path("post/",PostListView.as_view()),
	path("blog/<int:pk>/update/",BlogUpdateView.as_view()),
	path("post/<int:pk>/update/",PostUpdateView.as_view()),
	path("photocreate/",PhotoCreateView.as_view()),
	path("photo/<int:pk>/detail/",PhotoDetailView.as_view()),
	

]