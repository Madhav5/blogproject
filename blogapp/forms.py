from django import forms

from .models import *



class BlogForm(forms.ModelForm):
	class Meta:
		model=Blog
		fields=['title','image','description','author']




class PostForm(forms.ModelForm):
	class Meta:
		model=Post
		fields="__all__"


class PhotoForm(forms.ModelForm):
	class Meta:
		model=Photo
		fields="__all__"


class ContactForm(forms.Form):
	fullname=forms.CharField(widget=forms.TextInput(attrs={
		"class":"form-control",

		"placeholder":"Enter your full name",}))
	email=forms.EmailField(required=False,widget=forms.EmailInput(attrs={
		"class":"form-control",

		"placeholder":"Email",}))
	mobile=forms.CharField(widget=forms.NumberInput(attrs={
		"class":"form-control",

		"placeholder":"Phone number",}))
	subject=forms.CharField(widget=forms.TextInput(attrs={
		"class":"form-control",

		"placeholder":"Subject",}))
	message=forms.CharField(widget=forms.Textarea(attrs={
		"class":"form-control",

		"placeholder":"Type your message",
		"rows":5,

		}))




class RegistrationForm(forms.Form):
	email=forms.EmailField(widget=forms.EmailInput())
	password=forms.CharField(widget=forms.PasswordInput())
	confirm_password=forms.CharField(widget=forms.PasswordInput())










# class SurveyForm(forms.Form):
# 	fullname=forms.CharField(widget=forms.TextInput(attrs={
# 		"class":"form-control",

# 		"placeholder":"Enter your full name",}))

# 	email=forms.EmailField(required=False,widget=forms.EmailInput(attrs={
# 		"class":"form-control",

# 		"placeholder":"Email",}))

# 	How_old_are_you: =forms.CharField(widget=forms.NumberInput(attrs={
# 		"class":"form-control",

# 		"placeholder":"Enter your age",}))










